# ChatTweaks
ChatTweaks repo for the ChatTweaks plugin on BukkitDev / SpigotMC
Chat Tweaks is the ultimate chat plugin. It does everything you want it to do.
Be it creating channels for your mods, filter out unwanted advertisement, reduce spammers and clear your chat, ChatTweaks has it.
But ChatTweaks isn't all work! Create emoticons, create hashtags and AtSigns which alerts people,
replace words with long phrases for easy access, create Automatic messages to keep your players up-to-date,
or just add a bit of spicy colors in the chat with colored dollar signs and commands.
