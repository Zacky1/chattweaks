package com.griffinrealms.plugins.chattweaks.utils;

import java.util.HashMap;

import org.bukkit.Bukkit;

import com.griffinrealms.plugins.chattweaks.managers.Manager;

public class SpamDelay {

	private HashMap<String, Long> pList = new HashMap<String, Long>();
	private boolean filterLvl;
	private Manager manager;

	public SpamDelay(Manager man, boolean filter) {
		filterLvl = filter;
		manager = man;
	}

	public boolean runDelay(String player) {
		if (pList.containsKey(player)) {
			long b = pList.get(player);
			if ((System.currentTimeMillis() - b) > 1000) {
				pList.remove(player);
				return false;
			} else {
				if(filterLvl){
					manager.getMessager().showMessage(player, "Spam.RateLimit", null);
					return true;
				}else{
					Bukkit.getPlayer(player).kickPlayer(manager.getSpamConfig().getString("Rate.Kick Message"));
					return true;
				}
			}
		} else {
			pList.put(player, System.currentTimeMillis());
			return false;
		}
	}
}
