package com.griffinrealms.plugins.chattweaks.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.managers.Messenger;

public class Config {

	private Manager manager;
	private String configName;
	private File configFile;
	private boolean isAlive;
	private FileConfiguration configFileConf;

	public Config(Manager man, String cn) {
		manager = man;
		configName = cn;
		File des = manager.getChatTweaks().getDataFolder();
		if (!des.exists()) {
			des.mkdir();
		}
		isAlive = false;
		configFile = new File(des, configName + ".yml");
	}

	public void save() {
		String output = configFileConf.saveToString();
		output = output.replace("  ", "    ");

		while (output.indexOf('#') != -1) {
			output = output.substring(output.indexOf('\n', output.indexOf('#')) + 1);
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(manager.getChatTweaks().getResource(configName + ".yml")));
			HashMap<String, String> comments = new HashMap<String, String>();
			String temp = "";
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.contains("#")) {
					temp += line + "\n";
				} else if (line.contains(":")) {
					line = line.substring(0, line.indexOf(":") + 1);
					if (!temp.isEmpty()) {
						comments.put(line, temp);
						temp = "";
					}
				}
			}
			for (String key : comments.keySet()) {
				if (output.indexOf(key) != -1) {
					output = output.substring(0, output.indexOf(key))
							+ comments.get(key)
							+ output.substring(output.indexOf(key));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(manager.getChatTweaks().getDataFolder(), configName + ".yml")));
			writer.write(output);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createConfig() {
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				isAlive = true;
				loadFirstConfig();
			} catch (Exception e) {
				
			}
		}
	}

	public void deleteConfig() {
		if (configFile.exists()) {
			configFile.delete();
			isAlive = false;
			Messenger.showDebug("Deleted the " + configName + " config!");
		} else {
			Messenger.showDebug("Something tried to delete the " + configName + " config! But it didn't exist, aborting");
		}
	}

	public void load() {
		if (!isAlive) {
			createConfig();
		}
		configFileConf = YamlConfiguration.loadConfiguration(configFile);
	}

	private void loadFirstConfig() {
		try {
			OutputStream out = new FileOutputStream(configFile);
			InputStream is = manager.getChatTweaks().getResource(configName + ".yml");
			byte[] buf = new byte[1024];
			int len;
			while ((len = is.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			is.close();
			configFileConf = YamlConfiguration.loadConfiguration(configFile);
			Messenger.showDebug("Loaded default options to the Configuration: "+ configName);
		} catch (Exception e) {
			manager.getMessager().showConsoleMessage("ERROR", "Error creating the default config. Try deleting the file and trying again!");
		}
	}

	public void updateDefaults() {
		File temp = null;
		FileConfiguration tempConf = null;
		try {
			temp = new File(manager.getChatTweaks().getDataFolder(), "TempFile-CAN-DELETE-IF-SERVER-RESTARTED.yml");
			if (temp.exists()) {
				temp.delete();
			}
			temp.createNewFile();
			InputStream is = manager.getChatTweaks().getResource(configName + ".yml");

			Reader reader = new InputStreamReader(is);
			tempConf = YamlConfiguration.loadConfiguration(reader);
			for (String key : tempConf.getKeys(true)) {
				if (!configFileConf.contains(key)) {
					set(key, tempConf.get(key));
				}
			}
			set("Version", manager.getChatTweaks().currentversion);
			tempConf = null;
			temp.delete();
			save();
		} catch (Exception e) {
			manager.getMessager().showConsoleMessage("ERROR", "Something happened while updating config to the new version... Delete the file and try again?");
		}
	}

	public void set(String key, Object value) {
		configFileConf.set(key, value);
	}

	public FileConfiguration getConfig() {
		if (configFileConf != null) {
			return configFileConf;
		} else {
			return YamlConfiguration.loadConfiguration(configFile);
		}
	}

	public Object get(String key) {
		return configFileConf.get(key);
	}

	public String getString(String key) {
		return configFileConf.getString(key);
	}

	public Boolean getBoolean(String key) {
		return configFileConf.getBoolean(key);
	}

	public List<String> getStringList(String key) {
		return configFileConf.getStringList(key);
	}

	public Integer getInt(String key) {
		return configFileConf.getInt(key);
	}

	public Boolean isAlive() {
		return isAlive;
	}

}
