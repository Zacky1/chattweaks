package com.griffinrealms.plugins.chattweaks.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;

public class Helper {

	private ChatTweaks pl;

	public Helper(ChatTweaks ChatTweaks) {
		pl = ChatTweaks;
	}

	private int taskID = 0;
	private boolean isRunning;

	public void stop() {
		if (taskID != 0) {
			Bukkit.getServer().getScheduler().cancelTask(taskID);
		}
	}

	public boolean isStarted() {
		return isRunning;
	}

	/*----- update isRunning boolean -----*/
	public void updateThread() {
		Bukkit.getScheduler().runTaskTimerAsynchronously(pl, new Runnable() {
			@Override
			public void run() {
				Updater up = new Updater(49251,"91f0e006b2068e4338a43b2116be6394c89a2557");
				pl.newversion = up.query();
				int nv = pl.toInteger(pl.newversion);
				int od = pl.toInteger(pl.currentversion);
				if (od < nv) {
					pl.hasUpdate = true;
				} else {
					pl.hasUpdate = false;
				}
			}
		}, 100, 21600 * 20);
		isRunning = true;
	}

	public HashMap<String, Integer> set = new HashMap<String, Integer>();

	public HashMap<String, Integer> getSet() {
		return set;
	}

	public Integer getSetInt(String s) {
		return set.get(s);
	}

	public String getTop() {
		int maxV = (Collections.max(set.values()));
		for (Entry<String, Integer> entry : set.entrySet()) {
			if (entry.getValue() == maxV) {
				return entry.getKey();
			}
		}
		return null;
	}

	public void logToFile(File f, String s) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(f, true));
			writer.write(s.toLowerCase());
			writer.newLine();
			writer.flush();
			writer.close();
		} catch (Exception ex) {
			Bukkit.getLogger().log(Level.SEVERE,
					"[ChatTweaks] Unable to log to file! Error: " + ex);
		}
	}

	public void find(File f) {
		if (f == null) {
			return;
		}
		try {
			set.clear();
			BufferedReader bf = new BufferedReader(new FileReader(f));
			String nowL;
			if (!bf.ready()){
				bf.close();
				throw new IOException();
			}
			while ((nowL = bf.readLine()) != null) {
				if (set.containsKey(nowL)) {
					int i = set.get(nowL);
					i++;
					set.put(nowL, i);
				} else {
					set.put(nowL, 1);
				}
			}
			bf.close();
		} catch (Exception e) {
			Bukkit.getLogger().log(
					Level.SEVERE,
					"[ChatTweaks] Error in file " + f.getName() + "! Error:"
							+ e);
		}
	}

	public void deleteTopFile(File f) {
		try {
			if (!f.exists()) {
				Bukkit.getLogger().log(
						Level.SEVERE,
						"[ChatTweaks] Error file " + f.getName()
								+ " does not exist!");
			} else {
				try {
					if (f.isDirectory()) {

						if (f.list().length == 0) {
							f.delete();
						} else {
							String files[] = f.list();
							for (String temp : files) {
								File fileDelete = new File(f, temp);
								deleteTopFile(fileDelete);
							}
							if (f.list().length == 0) {
								f.delete();
							}
						}
					} else {
						f.delete();
					}
				} catch (Exception e) {
					Bukkit.getLogger().log(
							Level.SEVERE,
							"[ChatTweaks] Error deleting file" + f.getName()
									+ " Error: " + e);
				}
			}
		} catch (Exception ex) {
			Bukkit.getLogger().log(
					Level.SEVERE,
					"[ChatTweaks] Error with file" + f.getName() + " Error:"
							+ ex);
		}
	}

}
