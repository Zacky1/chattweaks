package com.griffinrealms.plugins.chattweaks.managers;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.SimplePluginManager;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;

public class PlayerManager implements Listener {

	private String logout, login, kick, whitelist, worldChange, serverFull, negative, positive;

	private Manager manager;

	public PlayerManager(Manager man) {
		manager = man;
	}

	public void load() {
		logout = manager.getMessagesConfig().getString("Messages.Connections.Logout");
		login = manager.getMessagesConfig().getString("Messages.Connections.Login");
		kick = manager.getMessagesConfig().getString("Messages.Connections.Kick");
		whitelist = manager.getMessagesConfig().getString("Messages.Connections.Whitelisted");
		serverFull = manager.getMessagesConfig().getString("Messages.Connections.FullServer");
		worldChange = manager.getMessagesConfig().getString("Messages.Connections.WorldChange");
		negative = manager.getMessagesConfig().getString("Messages.Connections.Negative");
		positive = manager.getMessagesConfig().getString("Messages.Connections.Positive");
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e) {
		if (login.equalsIgnoreCase("NONE")) {
			e.setJoinMessage(null);
			return;
		}
		Player p = e.getPlayer();
		if (p.isOp() && manager.getChatTweaks().hasUpdate) {
			p.sendMessage(ChatTweaks.prefix + "An update has been found!");
			p.sendMessage(ChatTweaks.prefix + "You have " + ChatTweaks.primaryColor + manager.getChatTweaks().currentversion
					+ ChatTweaks.secondaryColor + " while ChatTweaks has "+ ChatTweaks.primaryColor+ manager.getChatTweaks().newversion);
			p.sendMessage(ChatTweaks.prefix + "Update at: http://dev.bukkit.org/server-mods/chat-tweaks/");
		}
		if (manager.getMainConfig().getBoolean("Misc.Vanish Integration")) {
			for (MetadataValue v : p.getMetadata("vanished")) {
				if (v.asBoolean()) {
					Messenger.showDebug("Player " + p.getName()+ " is vanished. Hiding logging message");
					e.setJoinMessage(null);
					return;
				}
			}
		}
		String s = login;
		if (s.contains("%p%")) {
			s = s.replaceAll("%p%", p.getName());
		}
		s = ChatColor.translateAlternateColorCodes('&', s);
		e.setJoinMessage(positive + s);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (!(e.getPlayer() instanceof Player)) {
			Messenger.showDebug("Command sent was not from a player! Aborting checks.");
			return;
		}
		String ems = e.getMessage();
		if (ems.startsWith("/")) {
			ems = ems.split(" ")[0];
			ems = ems.replaceFirst("/", "");
			try {
				if (Bukkit.getServer().getPluginManager() instanceof SimplePluginManager) {
					final Field f = SimplePluginManager.class.getDeclaredField("commandMap");
					f.setAccessible(true);
					SimpleCommandMap cmdMap = (SimpleCommandMap) f.get(Bukkit.getServer().getPluginManager());
					if (cmdMap.getCommand(ems) == null) {
						manager.getMessager().showMessage(e.getPlayer().getName(),"Error.UnknownCommand", null);
						e.setCancelled(true);
					}
				}
			} catch (Exception ex) {
				manager.getMessager().showMessage(e.getPlayer().getName(), "Error.Unknown", null);
				manager.getMessager().showConsoleMessage("ERROR","A severe error occured during command lookups. If this problem persists, contact Zacky1 on BukkitDev");
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogout(PlayerQuitEvent e) {
		if (logout.equalsIgnoreCase("NONE")) {
			e.setQuitMessage(null);
			return;
		}
		String s = logout;
		if (s.contains("%p%")) {
			s = s.replaceAll("%p%", e.getPlayer().getName());
		}
		s = ChatColor.translateAlternateColorCodes('&', s);
		e.setQuitMessage(negative + s);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerKicked(PlayerKickEvent e) {
		if (kick.equalsIgnoreCase("NONE")) {
			e.setLeaveMessage(null);
			return;
		}
		String r = e.getReason();
		String s = kick;
		if (s.contains("%p")) {
			s = s.replace("%p%", e.getPlayer().getName());
		}
		if (s.contains("%cr%")) {
			s = s.replace("%cr%", r);
		}
		if (s.contains("%r%")) {
			s = s.replace("%r%", ChatColor.stripColor(r));
		}
		s = ChatColor.translateAlternateColorCodes('&', s);
		e.setLeaveMessage(negative + s);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPrelogin(PlayerLoginEvent e) {
		String s = null;
		if (e.getResult().equals(Result.KICK_FULL)) {
			if (serverFull.equalsIgnoreCase("NONE")) {
				e.setKickMessage("");
				return;
			}
			s = ChatColor.translateAlternateColorCodes('&', serverFull);
		}
		if (e.getResult().equals(Result.KICK_WHITELIST)) {
			if (whitelist.equalsIgnoreCase("NONE")) {
				e.setKickMessage("");
				return;
			}
			s = ChatColor.translateAlternateColorCodes('&', whitelist);
		}
		e.setKickMessage(s);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerWorld(PlayerChangedWorldEvent e) {
		if (worldChange.equalsIgnoreCase("NONE")) {
			return;
		}
		String s = worldChange;
		if (s.contains("%p")) {
			s = s.replace("%p%", e.getPlayer().getName());
		}
		if (s.contains("%w%")) {
			s = s.replace("%w%", e.getPlayer().getWorld().getName());
		}
		if (s.contains("%f%")) {
			s = s.replace("%f%", e.getFrom().getName());
		}
		s = ChatColor.translateAlternateColorCodes('&', s);
		Bukkit.broadcastMessage(positive + s);
	}
}
