package com.griffinrealms.plugins.chattweaks.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public class AutomaticMessagesManager {

	private Manager manager;
	private String prefix, header, footer;
	private int interval, taskId, currentIndex = 0;
	private Map<Integer, List<String>> messages = new HashMap<Integer, List<String>>();

	public AutomaticMessagesManager(Manager man) {
		manager = man;
	}

	public void load() {
		interval = manager.getMainConfig().getInt("AM.Interval");
		prefix = manager.getMainConfig().getString("AM.Prefix");
		header = manager.getMainConfig().getString("AM.Header");
		footer = manager.getMainConfig().getString("AM.Footer");
		if (prefix == null || prefix.equals("NONE")) {
			prefix = "";
		}
		if (header == null || header.equals("NONE")) {
			header = "";
		}
		if (footer == null || footer.equals("NONE")) {
			footer = "";
		}

		ConfigurationSection sec = manager.getMainConfig().getConfig()
				.getConfigurationSection("AM.IDs");
		if (sec != null) {
			int i = 0;
			for (String key : sec.getKeys(false)) {
				if (key != null) {
					messages.put(
							i,
							manager.getMainConfig().getStringList(
									"AM.IDs." + key));
					i++;
				}
			}
			Messenger.showDebug("[Configuration]Automatic messages have been enabled.");
		}
		startTask();
	}

	public void startTask() {
		taskId = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(manager.getChatTweaks(),
			new Runnable() {
				public void run() {
					broadcastMessage();
				}
			}, 20L, interval * 60 * 20L);
	}

	public void stopTask() {
		Bukkit.getServer().getScheduler().cancelTask(taskId);
	}

	public void broadcastMessage() {
		if (messages == null || messages.isEmpty()) {
			Messenger.showDebug("Automatic Messages was empty! Please set AM.Enable to false in the configuration file instead!");
			stopTask();
			return;
		}
		String message = "";
		if (currentIndex > messages.size()) {
			currentIndex = 0;
		}

		message = header + "\n";

		for (String s : messages.get(currentIndex)) {
			message += prefix + s + "\n";
		}

		message += footer;

		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
				message));
		currentIndex++;
	}

	public boolean isLoaded() {
		if (messages != null && !messages.isEmpty()) {
			return true;
		}
		return false;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public Map<Integer, List<String>> getMessages() {
		return messages;
	}
}
