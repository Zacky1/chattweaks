package com.griffinrealms.plugins.chattweaks.managers;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.griffinrealms.plugins.chattweaks.utils.SpamDelay;

public class SpamManager {

	private List<String> cussWords = new ArrayList<String>();
	private List<String> whitelistedLinks = new ArrayList<String>();
	private boolean whiteSpaces, duplicates, linkIps, caps, swearing;
	private Manager manager;
	private SpamDelay spamDelay;
	
	public SpamManager(Manager man) {
		manager = man;
	}
	
	public void load(){
		cussWords = manager.getSpamConfig().getStringList("Swearing.Swear Words");
		whitelistedLinks = manager.getSpamConfig().getStringList("LinksIPs.Whitelisted Links");
		spamDelay = new SpamDelay(manager, manager.getSpamConfig().getBoolean("Rates.Result"));
		whiteSpaces = manager.getSpamConfig().getBoolean("Replace.Whitespaces");
		duplicates = manager.getSpamConfig().getBoolean("Replace.Duplicates");
		linkIps = manager.getSpamConfig().getBoolean("LinksIPs.Enable");
		caps = manager.getSpamConfig().getBoolean("Caps.Enable");
		swearing = manager.getSpamConfig().getBoolean("Swearing.Enable");
	}
	
	public String runSpamChecks(String msg, Player player) {
		boolean f = spamDelay.runDelay(player.getName());
		if (f) {
			return null;
		}
		String m = msg;
		m = checkWhitespaces(m, player);
		m = checkDuplicates(m, player);
		m = checkLinks(m, player);
		m = checkCaps(m, player);
		m = checkSwear(m, player);
		return m;
	}

	private String checkWhitespaces(String msg, Player player) {
		if(msg == null){
			return null;
		}
		if (whiteSpaces && !player.hasPermission("ChatTweaks.OverrideWhiteSpace")) {
			return msg.replaceAll("\\s+", " ");
		}
		return msg;
	}

	private String checkDuplicates(String msg, Player player) {
		if(msg == null){
			return null;
		}
		if (duplicates && !player.hasPermission("ChatTweaks.OverrideDuplicates")) {
			return msg.replaceAll("([A-Za-z!?])\\1+\\1+", "$1");
		}
		return msg;
	}

	private String checkLinks(String msg, Player player) {
		if(msg == null){
			return null;
		}
		if (linkIps) {
			if (containsLink(msg) == 1 || containsLink(msg) == 2) {
				if (containsLink(msg) == 2) {
					manager.getMessager().showMessage(player.getName(), "Spam.Severe-UnAllowedLink", null);
					return null;
				} else {
					if (player.hasPermission("ChatTweaks.Links")) {
						msg = msg.toLowerCase();
						msg = msg.replaceAll("(!?http://)+", "");
						msg = msg.replaceAll("(!?https://)+", "");
						for (String s : getLinks(msg)) {
							Bukkit.broadcastMessage("Link: " + s);
							if (isWhitelistedLink(s)) {
								msg = msg.replace(s, manager.getSpamConfig().get("LinksIPs.Color")+ s + ChatColor.valueOf(ChatColor.getLastColors(msg)));
							} else {
								if (player.hasPermission("ChatTweaks.OverrideWhitelistedLinks")) {
									msg = msg.replace(s, manager.getSpamConfig().get("linksColor") + s + ChatColor.valueOf(ChatColor.getLastColors(msg)));
								} else {
									if (manager.getSpamConfig().getBoolean("LinksIPs.Replacer.Enable")) {
										msg = msg.replace(s,ChatColor.valueOf((String) manager.getSpamConfig().get("LinksIPs.Color"))
																+ (String) manager.getSpamConfig().get("LinksIPs.Replacer.ReplaceWith")
																+ ChatColor.valueOf(ChatColor.getLastColors(msg)));
									} else {
										manager.getMessager().showMessage(player.getName(), "Spam.UnAllowedLink", null);
										return null;
									}
								}
							}
						}
					} else {
						manager.getMessager().showMessage(player.getName(), "Spam.NoPermLinks", null);
						return null;
					}
				}
			}
			if (containsIP(msg)) {
				if (player.hasPermission("ChatTweaks.OverrideIP")) {
					/*
					 * for(String s : getIPs(msg)){ msg=msg.replace(s,
					 * getChatTweaks
					 * ().SpamConfig.get("LinksIPs.Color")+s+ChatColor.RESET); }
					 */
				} else {
					for (String s : getIPs(msg)) {
						if (manager.getSpamConfig().getBoolean("LinksIPs.Replacer.Enable")) {
							msg = msg.replace(s, ChatColor.valueOf((String) manager.getSpamConfig().get("LinksIPs.Color"))
													+ manager.getSpamConfig().getString("LinksIPs.Replacers.ReplaceWith")
													+ ChatColor.getLastColors(msg));
						} else {
							manager.getMessager().showMessage(player.getName(), "Spam.UnAllowedLink", s);
							msg = null;
						}
					}
				}
			}
		}
		return msg;
	}

	private String checkCaps(String msg, Player player) {
		if(caps && !player.hasPermission("ChatTweaks.OverrideCaps")){
			if (isCaps(msg)) {
				if (manager.getSpamConfig().getBoolean("Caps.To Lower Case")) {
					msg = msg.toLowerCase();
				} else {
					manager.getMessager().showMessage(player.getName(), "Spam.CapsLimit", null);;
					msg = null;
				}
			}
		}
		return msg;
	}

	private String checkSwear(String msg, Player player) {
		if(swearing && !player.hasPermission("ChatTweaks.OverrideSwear")){
			if(isSwear(msg)){
				switch (manager.getSpamConfig().getInt("Swearing.Severity")) {
				case 1:
					msg = removeSwear(msg);
				case 2:
					manager.getMessager().showMessage(player.getName(), "Spam.NoSwearing", null);
					msg = null;
				case 3:
					player.kickPlayer(manager.getSpamConfig().getString("Swearing.Kick Message"));
					msg = null;
				}
			}
		}
		return msg;
	}

	private Pattern linkp = Pattern.compile("(\\w+:\\w+@)?([a-zA-Z\\d.-]+\\.[A-Za-z]{2,4})(:\\d+)?(/.*)?");
	private Pattern IPP = Pattern.compile("(?:[0-9]{1,3}\\.){3}[0-9]{1,3}");
	private Pattern sfIP = Pattern.compile("(?:[0-9]{1,3}(\\.|\\,|\\-|\\(dot\\))){3}[0-9]{1,3}");
	private Pattern sflink = Pattern.compile("(\\w+:\\w+@)?([a-zA-Z\\d.-]+\\-|\\,|\\(dot\\)[A-Za-z]{2,4})(:\\d+)?(/.*)?");
	private Pattern sflink2 = Pattern.compile("[www|mc]+\\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\\-\\._\\?\\,\\'/\\\\\\+&amp;%\\$#\\=~])*$");

	private int containsLink(String msg) {
		int filterlvl = manager.getSpamConfig().getInt("LinksIPs.Filtering");
		if (filterlvl == 3) {
			Matcher lvl1 = linkp.matcher(msg);
			if (lvl1.find()) {
				return 1;
			} else {
				Matcher lvl2 = sflink.matcher(msg);
				if (lvl2.find()) {
					return 2;
				} else {
					msg = msg.replaceAll(" ", ".");
					Matcher lvl3 = sflink2.matcher(msg);
					if (lvl3.find()) {
						return 2;
					} else {
						return 0;
					}
				}
			}
		} else if (filterlvl == 2) {
			Matcher lvl1 = linkp.matcher(msg);
			if (lvl1.find()) {
				return 1;
			} else {
				Matcher lvl2 = sflink.matcher(msg);
				if (lvl2.find()) {
					return 2;
				} else {
					return 0;
				}
			}
		} else {
			Matcher m = linkp.matcher(msg);
			if (m.find()) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	private boolean containsIP(String msg) {
		if (manager.getSpamConfig().getInt("LinksIPs.Filtering") >= 2) {
			Matcher m = sfIP.matcher(msg);
			return m.find();
		} else {
			Matcher m = IPP.matcher(msg);
			return m.find();
		}
	}

	private Set<String> getIPs(String msg) {
		Matcher m = IPP.matcher(msg);
		Set<String> s = new HashSet<String>();
		while (m.find()) {
			s.add(m.group(0));
		}
		return s;
	}

	private Set<String> getLinks(String msg) {
		Matcher m = linkp.matcher(msg);
		Set<String> s = new HashSet<String>();
		while (m.find()) {
			s.add(m.group(0));
		}
		return s;
	}

	private boolean isWhitelistedLink(String link) {
		if (whitelistedLinks.contains(link)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isCaps(String msg) {
		if (msg.length() >= manager.getSpamConfig().getInt("Caps.Minimum Characters")) {
			double ucCh = 0.0D;
			for (char c : msg.toCharArray()) {
				if (Character.isUpperCase(c)) {
					ucCh += 1.0D;
				}
			}
			double d = (double) manager.getSpamConfig().get("Caps.Percent Caps");
			if ((ucCh / msg.length() * 100.0D) > d) {
				return true;
			}
		}
		return false;
	}

	private String removeSwear(String msg) {
		String t = msg.toLowerCase();
		String cleanT = Normalizer.normalize(t, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

		cleanT = cleanT.replaceAll("3", "e");
		cleanT = cleanT.replaceAll("0", "o");
		cleanT = cleanT.replaceAll("4", "a");
		cleanT = cleanT.replaceAll("@", "a");
		cleanT = cleanT.replaceAll("1", "l");

		for (String s : cussWords) {
			if (cleanT.contains(s)) {
				Pattern swear = Pattern.compile(s, Pattern.CASE_INSENSITIVE + Pattern.LITERAL);
				Matcher m = swear.matcher(msg);
				while (m.find()) {
					msg = m.replaceAll("~");
				}
			}
		}
		return msg;
	}

	private boolean isSwear(String msg) {
		String t = msg.toLowerCase();
		String cleanT = Normalizer.normalize(t, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

		cleanT = cleanT.replaceAll("3", "e");
		cleanT = cleanT.replaceAll("0", "o");
		cleanT = cleanT.replaceAll("4", "a");
		cleanT = cleanT.replaceAll("@", "a");
		cleanT = cleanT.replaceAll("1", "l");
		for (String s : cussWords) {
			if (cleanT.contains(s)) {
				return true;
			}
		}
		return false;
	}
}
