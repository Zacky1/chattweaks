package com.griffinrealms.plugins.chattweaks.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;

public class DeathManager implements Listener {

	public DeathManager(ChatTweaks ct) {
		plugin = ct;
	}

	private ChatTweaks plugin;

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		try {
			String world = e.getEntity().getWorld().getName();
			String playerDisplay = p.getDisplayName();
			String playerName = p.getName();
			String killerDisplay = "Unknown";
			String killerType = "unknown";
			String killerItemName = "Unknown";
			String killerName = "Unknown";
			String msg = e.getDeathMessage();

			if (p.getLastDamageCause() != null) {
				EntityDamageEvent ede = p.getLastDamageCause();
				if (ede instanceof EntityDamageByEntityEvent) {
					EntityDamageByEntityEvent edee = (EntityDamageByEntityEvent) ede;
					Entity killer = edee.getDamager();
					if (killer instanceof Player) {
						if (plugin.DeathConfig.getBoolean("Misc.Disable PVP Messages")) {
							e.setDeathMessage(null);
							return;
						}
						killerDisplay = ((Player) killer).getDisplayName();
						killerName = ((Player) killer).getName();
						killerType = "player";

						ItemStack is = ((Player) killer).getItemInHand();
						if (!(is == null)) {
							if (plugin.DeathConfig.getBoolean("Misc.Item Display Name")) {
								killerItemName = is.getType().name();
							} else {
								killerItemName = is.getItemMeta().getDisplayName();
							}
						} else {
							killerItemName = "bare fists";
						}
					} else if (killer instanceof Creature) {
						if (plugin.DeathConfig.getBoolean("Misc.Disable Mob Messages")) {
							e.setDeathMessage(null);
							return;
						}
						Creature killerc = (Creature) killer;

						killerDisplay = killerc.getCustomName();
						killerName = plugin.DeathConfig.getString(killerc.getType().name().toUpperCase());
						killerType = killerc.getType().name().toUpperCase();
					} else if (killer instanceof Block) {
						Block killerb = (Block) killer;
						killerName = killerb.getType().name();
						killerDisplay = killerb.getType().name();
						killerItemName = killerb.getType().name();
						killerType = "block";
					} else {
						if (killer.getType() == EntityType.PRIMED_TNT) {
							killerName = "TNT";
							killerDisplay = "Primed TNT";
							killerItemName = "TNT";
							killerType = killerName;
						} else if (killer.getType() == EntityType.ARROW) {
							Projectile prot = (Projectile) killer;
							@SuppressWarnings("deprecation")
							ProjectileSource psour = prot.getShooter();
							if (psour instanceof Player) {
								Player p2 = (Player) psour;
								if (plugin.DeathConfig
										.getBoolean("Misc.Disable PVP Messages")) {
									e.setDeathMessage(null);
									return;
								}
								killerDisplay = p2.getDisplayName();
								killerName = p2.getName();
								killerType = "player";

								ItemStack is = p2.getItemInHand();
								if (!(is == null)) {
									if (plugin.DeathConfig
											.getBoolean("Misc.Item Display Name")) {
										killerItemName = is.getType().name();
									} else {
										killerItemName = is.getItemMeta()
												.getDisplayName();
									}
								} else {
									killerItemName = "bare fists";
								}
							} else if (psour instanceof Creature) {
								if (plugin.DeathConfig
										.getBoolean("Misc.Disable Mob Messages")) {
									e.setDeathMessage(null);
									return;
								}
								Creature killerc = (Creature) psour;

								killerDisplay = killerc.getCustomName();
								killerName = plugin.DeathConfig
										.getString(killerc.getType().name()
												.toUpperCase());
								killerType = killerc.getType().name()
										.toUpperCase();
							} else if (psour instanceof Block) {
								Block killerb = (Block) psour;
								killerName = killerb.getType().name();
								killerDisplay = killerb.getType().name();
								killerItemName = killerb.getType().name();
								killerType = "block";
							}
						}
					}
				}
				String action = dc.name();
				List<String> syntax;
				if (action == "ENTITY_ATTACK") {
					syntax = plugin.DeathConfig.getStringList("Message Syntax."
							+ killerType.toLowerCase() + "_kills");
				} else {
					syntax = plugin.DeathConfig.getStringList("Message Syntax."
							+ action.toLowerCase());
				}
				if (syntax == null || syntax.isEmpty() || syntax.equals(null)) {
					syntax = plugin.DeathConfig
							.getStringList("Message Syntax.other");
				}

				Random ran = new Random();
				int ranInt = ran.nextInt(syntax.size());
				msg = syntax.get(ranInt);

				if (msg.equals(null)) {
					msg = "&c%player% died of unknown cause!";
				}

				Pattern pattern = Pattern.compile("%\\s*(\\S+?)\\s*%");
				Matcher matcher = pattern.matcher(msg);
				while (matcher.find()) {
					String m = matcher.group(0);
					List<String> actiondesc = plugin.DeathConfig
							.getStringList("Action." + m.replaceAll("%", ""));
					if (actiondesc != null && !actiondesc.isEmpty()) {
						String rep = actiondesc.get(ran.nextInt(actiondesc
								.size()));
						msg = msg.replaceAll(m, rep);
					}
					String rep = plugin.DeathConfig.getString("Entity Names."
							+ m.replaceAll("%", ""));
					if (rep != null) {
						msg = msg.replaceAll(m, rep);
					}
				}

				msg = msg.replaceAll("%player%", playerName);
				msg = msg.replaceAll("%playerDisplay%", playerDisplay);
				msg = msg.replaceAll("%killerDisplay%", killerDisplay);
				msg = msg.replaceAll("%killer%", killerName);
				msg = msg.replaceAll("%" + killerName + "%", killerName);
				msg = msg.replaceAll("%" + action + "%", "");
				msg = msg.replaceAll("%world%", world);
				msg = msg.replaceAll("%item%", killerItemName.toLowerCase()
						.replaceAll("_", " "));
				String pref = "";
				String temp = plugin.DeathConfig.getString("Misc.Prefix");
				if (temp != null && !(temp.equalsIgnoreCase("none"))) {
					pref = temp;
				}
				e.setDeathMessage(ChatColor.translateAlternateColorCodes('&',
						pref + msg));
			}
		} catch (Exception ex) {
			e.setDeathMessage(ChatColor.translateAlternateColorCodes('&', "&c"
					+ p.getName() + " died of unknown cause!"));
			List<String> list = new ArrayList<String>();
			list.add("A severe unhandled error occured while setting death message. ChatTweaks was able to recover, but it may have left damage.");
			list.add("Here is data on this error...");
			list.add("CT Error code: 1");
			list.add("CT Method: onPlayerDeath()");
			list.add("Error thrown: " + ex);
			list.add("Stacktrace: ");
			for (StackTraceElement ste : ex.getStackTrace()) {
				list.add("     " + ste.toString());
			}
			// .showReport(list);
		}
	}
}
