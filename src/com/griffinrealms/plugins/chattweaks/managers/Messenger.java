package com.griffinrealms.plugins.chattweaks.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;
import com.griffinrealms.plugins.chattweaks.channels.Channel;

public class Messenger {

	private String bar = "----------";

	private Sound broadcast, atSign, clearChat, channelJoin, channelLeave;

	private Logger log;
	private Manager manager;
	private List<String> playerDisabledSound = new ArrayList<String>();

	public Messenger(Manager man) {
		manager = man;
		log = Bukkit.getLogger();
	}

	public void load() {
		broadcast 	= Sound.valueOf(manager.getMainConfig().getString("Broadcast.Sound"));
		atSign 		= Sound.valueOf(manager.getMainConfig().getString("AtSign.Sound"));
		clearChat 	= Sound.valueOf(manager.getMainConfig().getString("Misc.Clear Chat Sound"));
		channelJoin = Sound.valueOf(manager.getMainConfig().getString("Channels.Join Sound"));
		channelLeave = Sound.valueOf(manager.getMainConfig().getString("Channels.Leave Sound"));
	}

	public static void showDebug(String s) {
		if (ChatTweaks.debug) {
			Bukkit.getLogger().log(Level.INFO, ChatTweaks.rawPrefix + "[DebugMode]" + s);
		}
	}
	
	public void sendChannelJoin(String name) {
		Player player = Bukkit.getPlayer(name);
		if (!playerDisabledSound.contains(name)) {
			player.playSound(player.getLocation(), channelJoin, 50.0F, 50.0F);
		}
	}

	public void sendChannelLeave(String name) {
		Player player = Bukkit.getPlayer(name);
		if (!playerDisabledSound.contains(name)) {
			player.playSound(player.getLocation(), channelLeave, 50.0F, 50.0F);
		}
	}
	
	public void sendPlayerSound(String pname, String sound) {
		Sound s = Sound.valueOf(sound);
		if (s == null) {
			showDebug("Sound sent to player is not a viable sound.");
			return;
		}
		Player player = Bukkit.getPlayer(pname);
		player.playSound(player.getLocation(), s, 50.0F, 50.0F);
	}

	public void sendBroadcastSound() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.playSound(player.getLocation(), broadcast, 50.0F, 50.0F);
		}
	}

	public void sendClearChatSound(String pname) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.playSound(player.getLocation(), clearChat, 50.0F, 50.0F);
		}
	}

	public void sendAtSound(String pname) {
		if (pname.equals("CHATTWEAKS_EVERYONE")) {
			for (Player player : Bukkit.getOnlinePlayers()) {
				player.playSound(player.getLocation(), atSign, 1, 5);
			}
		} else {
			Player player = Bukkit.getPlayer(pname);
			if (!playerDisabledSound.contains(pname)) {
				player.playSound(player.getLocation(), atSign, 50.0F, 50.0F);
			}
		}
	}

	public void showConsoleMessage(String messageCode, String variable) {
		if(messageCode == null){
			log.log(Level.INFO, ChatTweaks.rawPrefix + variable);
			return;
		}
		
		if (messageCode.equals("ERROR")) {
			log.log(Level.SEVERE,
					"####################################################");
			log.log(Level.SEVERE, ChatTweaks.rawPrefix + variable);
			log.log(Level.SEVERE,
					"####################################################");
			return;
		}

		for (String msg : getMessageByCode("CONSOLE", messageCode)) {
			log.log(Level.INFO, ChatTweaks.rawPrefix + msg.replaceAll("%v%", variable));
		}
		return;
	}

	public void broadcastMessage(String messageCode, String variable) {
		Bukkit.broadcastMessage(ChatTweaks.prefix+ getMessageByCode(null, messageCode)[0].replaceAll("%v%", variable));
	}

	public void showMessage(String player, String messageCode, String variable) {
		if (player.equalsIgnoreCase("CONSOLE")) {
			for (String string : getMessageByCode(player, messageCode)) {
				if (string.contains("%v%")) {
					string = string.replaceAll("%v%", variable);
				}
				log.log(Level.INFO, ChatTweaks.rawPrefix + string);
			}
		}
		Player pl = Bukkit.getPlayerExact(player);
		for (String string : getMessageByCode(player, messageCode)) {
			if (string.contains("%v%")) {
				string = string.replaceAll("%v%", ChatTweaks.primaryColor
						+ variable + ChatTweaks.secondaryColor);
			}
			pl.sendMessage(ChatTweaks.prefix + string);
		}
	}

	public String[] getMessageByCode(String player, String messageCode) {
		if (messageCode.equalsIgnoreCase("MENU.Main")) {
			if (player.equals("CONSOLE")) {
				return new String[] {
						"<>" + bar + "<ChatTweaks>" + bar + "<>",
						"Developed by Zacky1",
						"Version: " + manager.getChatTweaks().currentversion,
						"/ChatTweaks Top  -- Get the most trended hashtags and atsigns",
						"/ChatTweaks Info -- Get a list of all chat modificators",
						"/ChatTweaks ClearChat -- Clear the chat.",
						"/ChatTweaks Lockdown  -- Put the chat under lockdown mode.",
						"/ChatTweaks AutoMessage [Next | List] -- Broadcast the next message or get a list of all messages",
						"/ChatTweaks Reload -- Reload ChatTweaks",
						"/ChatTweaks PurgeData -- Delete all data stored by ChatTweaks",
						"/ChatTweaks EnableDebug -- Temporarily enables debug mode. Can't be turned off without a restart.",
						"/Channels -- All commands and information on Chat Channels",
						"/Ct -- Alias for /ChatTweaks" };
			}

			Player pl = Bukkit.getPlayerExact(player);
			String string[] = new String[13];
			string[0] = ChatTweaks.secondaryColor + "<>" + bar + "<"
					+ ChatTweaks.primaryColor + "ChatTweaks"
					+ ChatTweaks.secondaryColor + ">" + bar + "<>";

			string[1] = ChatTweaks.secondaryColor + "Developed by "
					+ ChatTweaks.primaryColor + "Zacky1";
			string[2] = ChatTweaks.secondaryColor + "Version: "
					+ ChatTweaks.primaryColor
					+ manager.getChatTweaks().currentversion;

			int i = 3;
			if (!pl.hasPermission("ChatTweaks.NoTop") || pl.isOp()) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks Top"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "Get the most trended hashtags and atsigns";
				i++;
			}
			if (!pl.hasPermission("ChatTweaks.NoInfoScreen") || pl.isOp()) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks Top"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "Get a list of all chat modificators";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.DisableSound")) {
				string[i] = ChatTweaks.primaryColor
						+ "/ChatTweaks DisableSound"
						+ ChatTweaks.secondaryColor
						+ " -- "
						+ ChatTweaks.primaryColor
						+ "Stop recieving sounds when called through the atsign";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.ClearPersonalChat")) {
				string[i] = ChatTweaks.primaryColor
						+ "/ChatTweaks ClearPersonalChat"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor + "Clear your personal Chat.";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.Broadcast")) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks Broadcast"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor + "Broadcast a message";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.AutoMessage")) {
				string[i] = ChatTweaks.primaryColor
						+ "/ChatTweaks AutoMessage"
						+ ChatTweaks.secondaryColor
						+ "["
						+ ChatTweaks.primaryColor
						+ "Next"
						+ ChatTweaks.secondaryColor
						+ " | "
						+ ChatTweaks.primaryColor
						+ "List"
						+ ChatTweaks.secondaryColor
						+ "] -- "
						+ ChatTweaks.primaryColor
						+ "Broadcast the next message or get a list of all messages";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.ClearChat")) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks ClearChat"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "Clear the chat for all players from spam.";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.LockdownMode")) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks Lockdown"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "Put the chat in lockdown mode.";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.reload")) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks Reload"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor + "Reload ChatTweaks";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.PurgeData")) {
				string[i] = ChatTweaks.primaryColor + "/ChatTweaks PurgeData"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "Delete all data stored by ChatTweaks";
				i++;
			}
			if (pl.hasPermission("ChatTweaks.EnableDebug")) {
				string[i] = ChatTweaks.primaryColor
						+ "/ChatTweaks EnableDebug"
						+ ChatTweaks.secondaryColor
						+ " -- "
						+ ChatTweaks.primaryColor
						+ "Temporarily enables debug mode. Can't be turned off without a restart.";
				i++;
			}
			if (manager.getMainConfig().getBoolean("Channels.Enable")) {
				string[i] = ChatTweaks.primaryColor + "/Channels"
						+ ChatTweaks.secondaryColor + " -- "
						+ ChatTweaks.primaryColor
						+ "All commands and information on Chat Channels";
				i++;
			}
			string[i] = ChatTweaks.primaryColor + "/Ct"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor + "Alias for /ChatTweaks";
			i++;

			return string;
		} else if (messageCode.equalsIgnoreCase("MENU.Channels")) {

			String channel[] = new String[13];
			channel[0] = ChatTweaks.secondaryColor + "<>" + bar + "<"
					+ ChatTweaks.primaryColor + "Channels"
					+ ChatTweaks.secondaryColor + ">" + bar + "<>";

			channel[1] = ChatTweaks.primaryColor + "Current Channel: ";
			if (player.equals("CONSOLE")) {
				channel[1] += ChatTweaks.secondaryColor
						+ "Cannot join channels as console.";
			} else {
				Player pl = Bukkit.getPlayerExact(player);
				channel[1] += ChatTweaks.secondaryColor + manager.getChannels().getChannelByPlayer(pl).getName();
			}

			channel[2] = ChatTweaks.primaryColor + "/Channels join <Channel>"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor + "Join the specified channel.";
			channel[3] = ChatTweaks.primaryColor + "/Channels add <Player>"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor
					+ "Add the player to your channel.";
			channel[4] = ChatTweaks.primaryColor + "/Channels kick <Player>"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor
					+ "Kick the player to your channel.";
			channel[5] = ChatTweaks.primaryColor + "/Channels leave"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor + "Leave the channel you are in.";
			channel[6] = ChatTweaks.primaryColor
					+ "/Channels move <Player> <Channel>"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor
					+ "Move the player to his new channel.";
			channel[7] = ChatTweaks.primaryColor + "/ch"
					+ ChatTweaks.secondaryColor + " -- "
					+ ChatTweaks.primaryColor + "Alias for /Channels";

			channel[8] = ChatTweaks.primaryColor + "List of Channels: ";

			int ind = 9;
			for (Channel channels : manager.getChannels()
					.getRegisteredChannels().values()) {
				channel[ind] = ChatTweaks.secondaryColor + "- "
						+ ChatTweaks.primaryColor + channels.getName()
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor + "Unjoinable"; /* TODO */
				ind++;
			}

		} else if (messageCode.equalsIgnoreCase("LIST.Channels")) {
			int i = manager.getChannels().getRegisteredChannels().size() + 3;
			String channel[] = new String[i];
			channel[0] = ChatTweaks.primaryColor + "<>" + bar + "<"
					+ ChatTweaks.secondaryColor + "Channels"
					+ ChatTweaks.primaryColor + ">" + bar + "<>";

			channel[1] = ChatTweaks.primaryColor + "Current Channel: ";
			if (player.equals("CONSOLE")) {
				channel[1] += ChatTweaks.secondaryColor
						+ "Cannot join channels as console.";
			} else {
				Player pl = Bukkit.getPlayerExact(player);
				channel[1] += ChatTweaks.secondaryColor+ manager.getChannels().getChannelByPlayer(pl).getName();
			}

			channel[2] = ChatTweaks.primaryColor + "List of Channels: ";
			int ind = 3;
			for (Channel channels : manager.getChannels()
					.getRegisteredChannels().values()) {
				channel[ind] = ChatTweaks.secondaryColor + "- "
						+ ChatTweaks.primaryColor + channels.getName()
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor + "Unjoinable"; /* TODO */
				ind++;
			}

			return channel;
		}
		return new String[] { manager.getMessagesConfig().getString(
				"Messages." + messageCode) };
	}

	public boolean toggleSound(String name) {
		// TODO Auto-generated method stub
		return false;
	}
}
