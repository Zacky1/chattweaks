package com.griffinrealms.plugins.chattweaks.managers;

import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

public class DeathMessagesManager implements Listener{
	
	private Manager manager;
	
	public DeathMessagesManager(Manager manager){
		this.manager = manager;
	}
	
	public void load(){
		
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDeath(PlayerDeathEvent e) {
		Player deadPlayer = e.getEntity();
		String world = e.getEntity().getWorld().getName();
		String deathType = "UNKNOWN";
		String killerType = "UNKNOWN";
		ItemStack killerHeldItem = null;
		String killerName = "UNKNOWN";
		
		if(deadPlayer.getLastDamageCause() != null){
			EntityDamageEvent damageEvent = deadPlayer.getLastDamageCause();
			
			if (damageEvent instanceof EntityDamageByEntityEvent) {
				
				EntityDamageByEntityEvent entityDamageEvent = (EntityDamageByEntityEvent) damageEvent;
				Entity killer = entityDamageEvent.getDamager();
				if(killer instanceof Player){
					killerType = "PLAYER";
					deathType = "PLAYER";
					killerHeldItem = ((Player)killer).getItemInHand();
					killerName = ((Player) killer).getName();
				}else if(killer instanceof Creature){
					deathType = "CREATURE";
					killerType = ((Creature)killer).getType().toString();
					killerHeldItem = ((Creature)killer).getEquipment().getItemInHand();
					killerName = ((Creature)killer).getCustomName();
				}else if (killer instanceof Block) {
					deathType = "BLOCK";
					killerType = ((Block)killer).getType().toString();
					killerName = ((Block)killer).getType().name();
				}else{
					if (killer.getType() == EntityType.PRIMED_TNT) {
						deathType = "BLOCK";
						killerName = "TNT";
						killerType = killerName;
					}else if(killer.getType() == EntityType.ARROW){
						Projectile prot = (Projectile) killer;
						ProjectileSource source = ((ProjectileSource)prot.getShooter());
						if(source instanceof Player){
							killerType = "PLAYER";
							deathType = "PLAYER-ARROW";
							killerHeldItem = ((Player)source).getItemInHand();
							killerName = ((Player) source).getName();
						}else if(source instanceof Creature){
							deathType = "CREATURE-ARROW";
							killerType = ((Creature)source).getType().toString();
							killerHeldItem = ((Creature)source).getEquipment().getItemInHand();
							killerName = ((Creature)source).getCustomName();
						}else if (source instanceof Block) {
							deathType = "BLOCK-ARROW";
							killerType = ((Block)source).getType().toString();
							killerName = ((Block)source).getType().name();
						}
					}
				}
			}
		}
		
		
		
		//End of info gathering.
		
	}

}
