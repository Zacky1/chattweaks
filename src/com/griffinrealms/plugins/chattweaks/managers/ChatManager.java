package com.griffinrealms.plugins.chattweaks.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatManager implements Listener {

	private Manager manager;
	private boolean inLockdown = false, doChatLog = false, doSpamCheck = false;
	private ChatColor atSign = ChatColor.YELLOW, hasSign = ChatColor.GREEN,
			dolSign = ChatColor.AQUA, cmdSign = ChatColor.DARK_PURPLE,
			repSign = ChatColor.RED;
	private List<String> chatLog = new ArrayList<String>(),
			hashLog = new ArrayList<String>();
	private HashMap<String, String> emoticons = new HashMap<String, String>(),
			replacers = new HashMap<String, String>();

	public ChatManager(Manager man) {
		manager = man;
	}

	public void load() {
		doChatLog = manager.getMainConfig().getBoolean("Misc.Chat logging");
		
		if(manager.getMainConfig().getBoolean("Emoticons.Enable")){
			ConfigurationSection emosec = (ConfigurationSection) manager.getMainConfig().get("Emoticons.List");
			if (emosec != null) {
				emoticons.clear();
				for (String key : emosec.getKeys(false)) {
					if (key != null) {
						emoticons.put(key, "\\"+manager.getMainConfig().getString("Emoticons.List."+key));
					}
				}
				Messenger.showDebug("[Configuration]Enabled Emoticons. Found "+ emoticons.size() + " different Emoticons!");
			}
		}
		
		if(manager.getMainConfig().getBoolean("Replacers.Enable")){
			ConfigurationSection sec = (ConfigurationSection) manager.getMainConfig().get("Replacers.List");
			if (sec != null) {
				replacers.clear();
				for (String key : sec.getKeys(false)) {
					if (key != null) {
						replacers.put(key, manager.getMainConfig().getString("Replacers.List."+key));
					}
				}
				Messenger.showDebug("[Configuration]Enabled Replacer text. Found "+ replacers.size() + " replacer text");
			}
		}
		
		
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
		final Player player = e.getPlayer();
		final Pattern pattern = Pattern.compile("(?:\\s|\\A)[@#$!/]+([A-Za-z0-9-_]+)");
		String message = e.getMessage();

		if (manager.getMainConfig().getBoolean("Channels.Enable")) {
			manager.getChannels().processChannelMessage(e, inLockdown);
		}
		
		if (inLockdown || (inLockdown && e.isCancelled())) {
			if (!player.hasPermission("ChatTweaks.LockDownOverride")) {
				manager.getMessager().showMessage(player.getName(), "Misc.InLockdown", null);
				e.setCancelled(true);
			}
		}

		//At this point, if channel is not allowed in lockdown, and the player doesnt have lockdown override,
		//The event will be cancelled, thus there's no point in doing spamchecks etc..
		if (e.isCancelled()) {
			return;
		}

		if (doSpamCheck) {
			message = manager.getSpam().runSpamChecks(message, player);
			if (message == null) {
				e.setCancelled(true);
				return;
			}
		}

		/*
		 * Are emoticons allowed?
		 */
		if (manager.getMainConfig().getBoolean("Emoticons.Enable") && player.hasPermission("ChatTweaks.Emoticons")) {
			message = emoticonPlacer(message);
		}

		/*
		 * Are players able to use & to change their chat color?
		 */
		if (manager.getMainConfig().getBoolean("Misc.Allow Colors") && player.hasPermission("ChatTweaks.ChatColors")) {
			message = ChatColor.translateAlternateColorCodes('&', message);
		}

		/*
		 * Do we remove dots before a slash sign?
		 */
		if (manager.getMainConfig().getBoolean("Command.Remove dots")) {
			if (message.contains("./")) {
				message.replaceAll("./", "/");
			}
		}

		Matcher matcher = pattern.matcher(message);
		String word;
		while (matcher.find()) {
			word = matcher.group(0);
			word = word.trim();

			/*
			 * A word has been found for coloring.
			 */
			String last = ChatColor.getLastColors(message);
			if (word.startsWith("#")) {
				message.replaceAll(word, hashTag(player, word, last));
			} else if (word.startsWith("@")) {
				message.replaceAll(word, atSign(player, word, last));
			} else if (word.startsWith("!")) {
				message.replaceAll(word, replacer(player, word, last));
			} else if (word.startsWith("/")) {
				message.replaceAll(word, commands(player, word, last));
			}
		}
		chatLog(player.getName(), message);

		e.setMessage(message);
	}

	public void chatLog(String player, String message) {
		if (doChatLog) {
			chatLog.add(ChatColor.stripColor(player + ": " + message));
		}
	}

	public String dollars(Player player, String word, String last) {
		if (!manager.getMainConfig().getBoolean("Dollars.Enable")) {
			return word;
		}
		try {
			word = word.replaceFirst("\\$", "");
			int i = Integer.parseInt(word);
			return dolSign + "$" + i + ChatColor.valueOf(last.toUpperCase());
		} catch (NumberFormatException nume) {
			manager.getMessager().showMessage(player.getName(), "Error.NumberException", null);
			Messenger.showDebug("Player tried entering a non number preceded by a $ sign.");
		}
		return word;
	}

	public String commands(Player player, String word, String last) {
		if (!manager.getMainConfig().getBoolean("Command.Enable")) {
			return word;
		}
		// Replace dashes with a space for more readability in chat..
		word.replaceAll("-", " ");
		return cmdSign + word + ChatColor.valueOf(last.toUpperCase());
	}

	public String replacer(Player player, String word, String last) {
		if (!manager.getMainConfig().getBoolean("Replacers.Enable")) {
			return word;
		}
		for (String s : replacers.keySet()) {
			if (s.equalsIgnoreCase(word.substring(1))) {
				return repSign + word + ChatColor.valueOf(last.toUpperCase());
			}
		}
		return word;
	}

	public String atSign(Player player, String word, String last) {
		if (!manager.getMainConfig().getBoolean("AtSign.Enable")) {
			return word;
		}

		if (word.length() >= 16 || word.length() <= 2) {
			manager.getMessager().showMessage(player.getName(),
					"Misc.TooLittleBig", word);
			return word;
		}

		if (word.equalsIgnoreCase("@Everyone") && player.hasPermission("ChatTweaks.AtEveryone")) {
			manager.getMessager().sendAtSound("CHATTWEAKS_EVERYONE");
		} else {
			if (manager.getMainConfig().getBoolean("AtSign.Only Usernames")) {
				Player p1 = Bukkit.getPlayer(word.substring(1));
				if (p1 != null) {
					// We put it back because the playername could be
					// incomplete.
					// Ex: Someone types @Zack would then turn into @Zacky1
					word = "@" + p1.getName();
				} else {
					Messenger.showDebug("Player atsign: Player " + word + " is not online");
					word = "~";
				}
			}
		}
		return atSign + word + ChatColor.valueOf(last.toUpperCase());
	}

	public String hashTag(Player player, String word, String last) {
		if (!manager.getMainConfig().getBoolean("HashTags.Enable")) {
			return word;
		}

		if (word.length() >= 16 || word.length() <= 2) {
			manager.getMessager().showMessage(player.getName(),
					"Misc.TooLittleBig", word);
			return word;
		}
		hashLog.add(word);
		return hasSign + word + ChatColor.valueOf(last.toUpperCase());
	}

	public String emoticonPlacer(String message) {
		for (String s : emoticons.keySet()) {
			if (message.contains(s)) {
				message.replaceAll(Pattern.quote(s), Matcher.quoteReplacement(emoticons.get(s)));
			}
		}
		return message;
	}

	public boolean toggleLockdown() {
		if(inLockdown){
			inLockdown = false;
			manager.getMessager().broadcastMessage("Misc.OutLockdown", null);
		}else{
			inLockdown = true;
			manager.getMessager().broadcastMessage("Misc.InLockdown", null);
		}
		// m.broadcastMessage("Misc.OutLockdown", name); InLockdown
		return inLockdown;
	}
}
