package com.griffinrealms.plugins.chattweaks.managers;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;
import com.griffinrealms.plugins.chattweaks.channels.ChannelManager;
import com.griffinrealms.plugins.chattweaks.utils.Config;

public class Manager {

	private Messenger messenger;
	private SpamManager spam;
	private ChannelManager channels;
	private DeathMessagesManager death;
	private ChatManager chat;
	private PlayerManager player;
	private AutomaticMessagesManager automessage;

	private ChatTweaks plugin;
	private Config messageConfig, spamConfig, mainConfig, deathConfig;

	public Manager(ChatTweaks pl) {
		plugin = pl;
		/*
		 * Must be initialized before others because Config could generate
		 * errors.
		 */
		messenger = new Messenger(this);

		mainConfig = new Config(this, "Configuration");
		spamConfig = new Config(this, "SpamConfig");
		deathConfig = new Config(this, "Deathconfig");
		messageConfig = new Config(this, "Messages");

		spam = new SpamManager(this);
		death = new DeathMessagesManager(this);
		channels = new ChannelManager(this);
		player = new PlayerManager(this);
		chat = new ChatManager(this);
		automessage = new AutomaticMessagesManager(this);

		reload();
	}

	public void disable() {
		messenger = null;
		spam = null;
		channels = null;
		death = null;
		chat = null;
		player = null;
		automessage = null;
	}

	public void reload() {
		Messenger.showDebug("Starting reload");

		/* Things that must be stopped before reload */
		automessage.stopTask();

		/* Config reloads */
		reload("MAIN");
		reload("MESSAGES");

		/* Manager loads */
		automessage.load();
		messenger.load();
		chat.load();
		player.load();
		channels.load();
		
		if(mainConfig.getBoolean("Misc.Enable Spam Configuration")){
			reload("SPAM");
			spam.load();
		}
		
		if(mainConfig.getBoolean("Misc.Enable Death Messages")){
			reload("DEATH");
			death.load();
		}

		Messenger.showDebug("Reload complete");
	}

	public void reload(String config) {
		Messenger.showDebug("Reloading Config " + config);
		switch (config) {
		case "MAIN": {
			mainConfig.load();
			if (mainConfig.getBoolean("Misc.Debug")) {
				ChatTweaks.debug = true;
				Messenger.showDebug("Debug mode enabled via config!");
			}
		}
		case "SPAM": {
			spamConfig.load();
		}
		case "MESSAGES": {
			messageConfig.load();
		}
		case "DEATH": {
			deathConfig.load();
		}
		}
	}

	public ChatTweaks getChatTweaks() {
		return plugin;
	}

	public Messenger getMessager() {
		return messenger;
	}

	public SpamManager getSpam() {
		return spam;
	}

	public ChannelManager getChannels() {
		return channels;
	}

	public ChatManager getChat() {
		return chat;
	}

	public AutomaticMessagesManager getAutoMessages() {
		return automessage;
	}

	public PlayerManager getPlayers() {
		return player;
	}

	public Config getMessagesConfig() {
		return messageConfig;
	}

	public Config getSpamConfig() {
		return spamConfig;
	}

	public Config getMainConfig() {
		return mainConfig;
	}

	public Config getDeathConfig() {
		return deathConfig;
	}
}
