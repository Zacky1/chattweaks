package com.griffinrealms.plugins.chattweaks.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.griffinrealms.plugins.chattweaks.channels.Channel;
import com.griffinrealms.plugins.chattweaks.channels.ChannelManager;
import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.managers.Messenger;

public class ChannelCmd implements CommandExecutor {

	private Messenger m;
	private Manager manager;
	private ChannelManager chh;

	public ChannelCmd(Manager manager) {
		this.manager = manager;
		m = manager.getMessager();
		chh = manager.getChannels();
	}
	
	private void joinCmd(Player player, Channel channel, Channel joinChannel){
		if(joinChannel.isPrivate()){
			if(!player.hasPermission("ChatTweaks.Channels."+joinChannel.getName()+".Join")){
				m.showMessage(player.getName(), "Channel.Error.Private", joinChannel.getName());
				return;
			}
		}
		if(channel != null){
			chh.unregisterPlayer(channel, player);
		}
		chh.registerPlayer(joinChannel, player);
		return;
	}
	
	private void addCmd(Player player, Channel channel, String playerToBeAdded){
		if(!channel.isPrivate()){
			m.showMessage(player.getName(), "Channel.Error.NotPrivate", channel.getName());
			return;
		}
		
		if(!player.hasPermission("ChatTweaks.Channels."+channel.getName()+".Add")){
			m.showMessage(player.getName(), "Channel.Error.Add", channel.getName());
			return;
		}
		
		Player addedPlayer = Bukkit.getPlayer(playerToBeAdded);
		if(addedPlayer == null){
			m.showMessage(player.getName(), "Error.PlayerException", playerToBeAdded);
			return;
		}
		chh.unregisterPlayerFromAll(addedPlayer);
		chh.registerPlayer(channel, addedPlayer);
	}
	
	private void moveCmd(Player player, Channel channel, String playerToBeMoved){
		Player movedPlayer = Bukkit.getPlayer(playerToBeMoved);
		if(movedPlayer == null){
			m.showMessage(player.getName(), "Error.PlayerException", playerToBeMoved);
			return;
		}
		
		if(channel.isPrivate()){
			if(!player.hasPermission("ChatTweaks.Channels."+channel.getName()+".Add")){
				m.showMessage(player.getName(), "Channel.Error.Move", "private");
				return;
			}
		}else{
			if(!player.hasPermission("ChatTweaks.Channels.Move")){
				m.showMessage(player.getName(), "Error.NoPerm", "private");
				return;
			}
		}
		
		chh.unregisterPlayerFromAll(movedPlayer);
		chh.registerPlayer(channel, movedPlayer);
		m.showMessage(movedPlayer.getName(), "Channel.Moved", channel.getName());
		m.showMessage(player.getName(), "Channel.MovedPlayer", movedPlayer.getName());
	}
	
	private void kickCmd(Player player, String playerToBeKicked){		
		Player kickedPlayer = Bukkit.getPlayer(playerToBeKicked);
		if(kickedPlayer == null){
			m.showMessage(player.getName(), "Error.PlayerException", playerToBeKicked);
			return;
		}
		Channel channel = chh.getChannelByPlayer(kickedPlayer);
		
		if(!player.hasPermission("ChatTweaks.Channels."+channel.getName()+".Kick")){
			m.showMessage(player.getName(), "Channel.Error.Kick", channel.getName());
			return;
		}
		
		if(channel.equals(chh.getDefaultChannel())){
			m.showMessage(player.getName(), "Channel.Error.NoLeave2", channel.getName());
			return;
		}
		
		chh.unregisterPlayer(channel, kickedPlayer);
	}
	
	private void leaveCmd(Player player, Channel channel){
		if (!channel.isDefault()) {
			chh.unregisterPlayer(channel, player);
			chh.registerPlayer(chh.getDefaultChannel(), player);
		} else {
			m.showMessage(player.getName(), "Channel.Error.NoLeave", null);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!manager.getMainConfig().getBoolean("Channels.Enable")) {
			m.showMessage(sender.getName(), "Misc.DisabledFeature", "Channels");
			return true;
		}
		if (!(sender instanceof Player)) {
			m.showMessage("CONSOLE", null, "You can't view this command in console.");
			m.showMessage("CONSOLE", "Channel.Main", null);
			return true;
		}
		Channel playerChannel = chh.getChannelByPlayer((Player)sender);
		if (args.length == 0) {
			m.showMessage(sender.getName(), "Channel.Main", null);
		} else {
			if (args[0].equalsIgnoreCase("join") || args[0].equalsIgnoreCase("j")) {
				if (args.length == 2) {
					Channel channel = chh.getChannelByName(args[1]);
					if(channel == null){
						m.showMessage(sender.getName(), "Channel.Error.NotFound", args[1]);
						m.showMessage(sender.getName(), "Channel.List", null);
						return true;
					}
					joinCmd((Player)sender, playerChannel, channel);
					return true;
				}
			} else if (args[0].equalsIgnoreCase("leave") || args[0].equalsIgnoreCase("l")) {
				leaveCmd((Player)sender, playerChannel);
				return true;
			} else if (args[0].equalsIgnoreCase("List") || args[0].equalsIgnoreCase("Info")) {
				m.showMessage(sender.getName(), "Channel.List", null);
				return true;
			} else if (args[0].equalsIgnoreCase("add")) {
				if (args.length == 2) {
					Channel channel = chh.getChannelByPlayer((Player)sender);
					if(channel == null){
						m.showMessage(sender.getName(), "Channel.Error.NotFound", args[1]);
						m.showMessage(sender.getName(), "Channel.List", null);
						return true;
					}
					addCmd((Player)sender, channel, args[1]);
					return true;
				}
			} else if (args[0].equalsIgnoreCase("kick")) {
				if (args.length == 2) {
					kickCmd((Player)sender, args[1]);
					return true;
				}
			} else if (args[0].equalsIgnoreCase("move")) {
				if (args.length == 3) {
					Channel channel = chh.getChannelByName(args[2]);
					if(channel == null){
						m.showMessage(sender.getName(), "Channel.Error.NotFound", args[1]);
						m.showMessage(sender.getName(), "Channel.List", null);
						return true;
					}
					moveCmd((Player)sender, channel, args[1]);
					return true;
				}
			} else {
				m.showMessage(sender.getName(), "Error.WrongCmd", null);
				m.showMessage(sender.getName(), "MainScreen", null);
			}
		}
		return true;
	}

}
