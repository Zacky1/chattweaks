package com.griffinrealms.plugins.chattweaks.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.griffinrealms.plugins.chattweaks.ChatTweaks;
import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.managers.Messenger;

public class ChatTweaksCmd implements CommandExecutor {

	private Manager manager;
	private Messenger m;
	private ChatColor atColor, hashColor, commandColor, dollarColor, replacerColor;
	private String bar = "----------";

	public ChatTweaksCmd(Manager man) {
		manager = man;
		m = manager.getMessager();
	}

	public void load() {
		atColor = ChatColor.valueOf(manager.getMainConfig().getString("AtSign.Color"));
		hashColor = ChatColor.valueOf(manager.getMainConfig().getString("HashTags.Color"));
		commandColor = ChatColor.valueOf(manager.getMainConfig().getString("Command.Color"));
		dollarColor = ChatColor.valueOf(manager.getMainConfig().getString("Dollars.Color"));
		replacerColor = ChatColor.valueOf(manager.getMainConfig().getString("Replacers.Color"));
	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (args.length == 0) {
			m.showMessage(sender.getName(), "MainScreen", null);
			return true;
		}
		if (args[0].equalsIgnoreCase("AM")|| args[0].equalsIgnoreCase("AutoMessage")) {
			if (!manager.getAutoMessages().isLoaded()) {
				m.showMessage(sender.getName(), "Error.DisabledFeature","Automatic Messages");
				return true;
			}
			if (sender.hasPermission("ChatTweaks.AutoMessage")) {
				if (args.length == 1 || args.length == 2
						&& args[1].equalsIgnoreCase("List")) {
					sender.sendMessage(ChatTweaks.secondaryColor + "<>" + bar+ "<" + ChatTweaks.primaryColor + "Auto Messages"+ ChatTweaks.secondaryColor + ">" + bar + "<>");
					sender.sendMessage(ChatTweaks.secondaryColor+ "List of auto messages:");
					int i = 1;
					for (int index : manager.getAutoMessages().getMessages().keySet()) {
						for (String str : manager.getAutoMessages().getMessages().get(index)) {
							str = ChatColor.translateAlternateColorCodes('&',str);
							sender.sendMessage(ChatTweaks.secondaryColor + " "+ i + ") " + ChatTweaks.primaryColor + str);
						}
						i++;
					}
				} else if (args.length == 2 && args[1].equalsIgnoreCase("Next")) {
					manager.getAutoMessages().broadcastMessage();
				}
				return true;
			}
		} else if (args[0].equalsIgnoreCase("broadcast") || args[0].equalsIgnoreCase("bc")) {
			if (sender.hasPermission("ChatTweaks.Broadcast")) {
				String msg = "";
				for (String s : args) {
					if (!(s.equalsIgnoreCase("broadcast") || s.equalsIgnoreCase("bc"))) {
						msg = msg + " " + s;
					}
				}
				String f = manager.getMainConfig().getString("Broadcast.Format");
				msg = f.replaceAll("%msg%", msg);
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes( '&', msg));
				m.sendBroadcastSound();
				return true;
			}
		} else if (args[0].equalsIgnoreCase("enableDebug")) {
			if (sender.hasPermission("ChatTweaks.EnableDebug")) {
				ChatTweaks.debug = true;
				m.showMessage(sender.getName(), "Misc.EnabledFeature", "Debug Mode");
				return true;
			}
		} else if (args[0].equalsIgnoreCase("DisableSound")
				|| args[0].equalsIgnoreCase("DS")) {
			if (sender.hasPermission("ChatTweaks.DisableSound")) {
				if (m.toggleSound(sender.getName())) {
					m.showMessage(sender.getName(), "Misc.DisableSound", "enabled");
				} else {
					m.showMessage(sender.getName(), "Misc.DisableSound", "disabled");
				}
				m.sendPlayerSound(sender.getName(), "DisSound");
				return true;
			}
		} else if (args[0].equalsIgnoreCase("lockdown")) {
			if (sender.hasPermission("ChatTweaks.LockdownMode")) {
				if (manager.getChat().toggleLockdown()) {
					m.showMessage(sender.getName(), "Misc.LockdownEnable", "enabled");
				} else {
					m.showMessage(sender.getName(), "Misc.LockdownEnable", "disabled");
				}
				return true;
			}
		} else if (args[0].equalsIgnoreCase("ClearChat")
				|| args[0].equalsIgnoreCase("CC")) {
			if (sender.hasPermission("ChatTweaks.ClearChat")) {
				for (Player p : Bukkit.getOnlinePlayers()) {
					for (int i = 0; i < 120; i++) {
						p.sendMessage(" ");
					}
				}
				m.broadcastMessage("Misc.ClearChat", sender.getName());
				m.sendBroadcastSound();
				return true;
			}
		} else if (args[0].equalsIgnoreCase("ClearPersonalChat")
				|| args[0].equalsIgnoreCase("CPC")) {
			if (sender.hasPermission("ChatTweaks.ClearPersonalChat")) {
				for (int i = 0; i < 120; i++) {
					sender.sendMessage(" ");
				}
				m.showMessage(sender.getName(), "Misc.ClearPChat", null);
				return true;
			}
		} else if (args[0].equalsIgnoreCase("Info")) {
			if (!sender.hasPermission("NoInfoScreen") || sender.isOp()) {
				sender.sendMessage(ChatTweaks.secondaryColor + "<>" + bar + "<"
						+ ChatTweaks.primaryColor + "Possibilities"
						+ ChatTweaks.secondaryColor + ">" + bar + "<>");
				sender.sendMessage(atColor
						+ "@"
						+ ChatTweaks.secondaryColor
						+ " - "
						+ ChatTweaks.primaryColor
						+ " Prefix a playername with @ and it will send him a sound.");
				sender.sendMessage(hashColor + "#" + ChatTweaks.secondaryColor
						+ " - " + ChatTweaks.primaryColor
						+ " Prefix words with # and it will make it a category");
				sender.sendMessage(commandColor + "/"
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor
						+ " Prefix commands with a / will color them.");
				sender.sendMessage(dollarColor + "$"
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor
						+ " Prefix dollors with a $ will color them.");
				sender.sendMessage(replacerColor + "!"
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor
						+ " Replace words starting with ! with important info.");
				sender.sendMessage(replacerColor + "!example"
						+ ChatTweaks.secondaryColor + " - "
						+ ChatTweaks.primaryColor + " will give you "
						+ replacerColor + "Example-Text");
				sender.sendMessage(atColor
						+ "@Everyone"
						+ ChatTweaks.secondaryColor
						+ " - "
						+ ChatTweaks.primaryColor
						+ " If this option is enabled, you will be able to send a sound to everyone");
				sender.sendMessage(ChatTweaks.secondaryColor + "<>" + bar
						+ "<End>" + bar + "<>");
				return true;
			}
		} else if (args[0].equalsIgnoreCase("PurgeData")
				|| args[0].equalsIgnoreCase("PD")) {
			if (sender.hasPermission("ChatTweaks.PurgeData")) {
				m.showMessage(sender.getName(), "Error.DisabledFeature",
						"Data purges");
				return true;
				/*
				 * try { File f = new
				 * File(pl.getDataFolder()+File.separator+"data");
				 * pl.helper.deleteTopFile(f);
				 * m.showMsg("Misc.PurgeDataSuccess", null, sender.getName()); }
				 * catch (Exception e) { m.showMsg("ERROR",
				 * "Error purging data!", sender.getName()); }
				 */
			}
		} else if (args[0].equalsIgnoreCase("reload")) {
			if (sender.hasPermission("ChatTweaks.reload")) {
				manager.reload();
				m.showMessage(sender.getName(), "Misc.ReloadConfSuccess", null);
				return true;
			}
		} else if (args[0].equalsIgnoreCase("top")) {
			m.showMessage(sender.getName(), "Error.DisabledFeature",
					"Top charts");
			return true;
			/*
			 * if(sender.hasPermission("ChatTweaks.NoTop") && !sender.isOp()){
			 * m.showMsg("Error.NoPerm", null, sender.getName()); return true; }
			 * sender
			 * .sendMessage(a+"<>"+bar+a+"<"+g+"ChatTweaks Top Trends"+a+">"
			 * +bar+a+"<>");
			 * if(pl.Configuration.getBoolean("HashTags.Count tracker")){ File f
			 * = new
			 * File(pl.getDataFolder()+File.separator+"data",File.separator
			 * +"HashTagSignLogs.txt"); pl.helper.find(f);
			 * 
			 * sender.sendMessage(a+"<"+g+"Local Hashtag"+a+">");
			 * if(f.exists()){ if(pl.helper.getSet() != null){ String i =
			 * pl.helper.getTop();
			 * sender.sendMessage(a+"Most treneded local hashtag: "+
			 * g+i+a+" entered " + g+pl.helper.getSetInt(i) +a+" times.");
			 * }else{ m.showMsg("Error.Unknown", null, sender.getName());
			 * m.showMsg("ERROR", "Hashtag set is null!", null); } }else{
			 * sender.
			 * sendMessage(a+pl.prefix+"No data recorded for local hashtags!");
			 * } /*sender.sendMessage(a+"<"+g+"Global Hashtags"+a+">"); String s
			 * = ""; for(String ghash : pl.ghash.g()) { s=s+g+ghash+a+", "; }
			 */
			/*
			 * }else{ m.showMsg("Misc.DisabledFeature", "HashTag counter",
			 * sender.getName()); }
			 * if(pl.Configuration.getBoolean("AtSign.Count tracker")){ File f =
			 * new File(pl.getDataFolder()+File.separator+"data",File.separator+
			 * "AtSignLogs.txt"); pl.helper.find(f); if(f.exists()){
			 * if(pl.helper.getSet() != null){ String i = pl.helper.getTop();
			 * sender.sendMessage(a+"Most called person: "+ g+i+a+" entered " +
			 * g+pl.helper.getSetInt(i) +a+" times."); }else{
			 * m.showMsg("Error.Unknown", null, sender.getName()); } }else{
			 * sender
			 * .sendMessage(a+pl.prefix+"No data recorded for local atsigns!");
			 * }
			 * 
			 * }else{ m.showMsg("Misc.DisabledFeature", "AtSigns counter",
			 * sender.getName()); }
			 */
		} else {
			m.showMessage(sender.getName(), "Error.WrongCmd", null);
			m.showMessage(sender.getName(), "MainScreen", null);
			return true;
		}
		// You only get here after the if-else, and if you don't have
		// permissions.
		m.showMessage(sender.getName(), "Error.NoPerm", null);
		return true;
	}
}
