package com.griffinrealms.plugins.chattweaks.metrics;

import java.io.IOException;

import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.managers.Messenger;
import com.griffinrealms.plugins.chattweaks.metrics.Metrics.Graph;

public class PlMetrics {
	private Manager manager;

	public PlMetrics(Manager plugin) {
		manager = plugin;
	}

	public void main(boolean f) {
		if (f) {
			try {
				Metrics metrics = new Metrics(manager.getChatTweaks());
				if (metrics.isOptOut()) {
					Messenger.showDebug("Metrics is opted out! This means no data was sent. If you want to change this, goto plugins/Metrics/Config.yml");
					return;
				}
				Graph channel = metrics.createGraph("Channel integration");
				channel.addPlotter(new Metrics.Plotter("Yes") {
					@Override
					public int getValue() {
						if (manager.getMainConfig().getBoolean("Channels.Enable")) {
							return 1;
						} else {
							return 0;
						}
					}
				});
				
				Graph spammer = metrics.createGraph("Spam integration");
				spammer.addPlotter(new Metrics.Plotter("Yes") {
					@Override
					public int getValue() {
						if (manager.getMainConfig().getBoolean("Misc.Enable Spam Configuration")) {
							return 1;
						} else {
							return 0;
						}
					}
				});
				
				Graph death = metrics.createGraph("Death integration");
				death.addPlotter(new Metrics.Plotter("Yes") {
					@Override
					public int getValue() {
						if (manager.getMainConfig().getBoolean("Misc.Enable Death Configuration")) {
							return 1;
						} else {
							return 0;
						}
					}
				});

				metrics.start();
			} catch (IOException e) {
				manager.getMessager().showConsoleMessage("ERROR", "ERROR Occured while sending Stats to McStats! E: "+e);
			}
		} else {
			Messenger.showDebug("McStats disabled in config. Ignoring");
		}
	}
}
