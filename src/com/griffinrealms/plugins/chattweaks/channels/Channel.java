package com.griffinrealms.plugins.chattweaks.channels;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.griffinrealms.plugins.chattweaks.managers.Messenger;

public class Channel{

	private String name;
	private String range;
	private String format;
	private boolean isPrivate;
	private boolean allowInLockdown;
	private boolean isDefault;
	private List<Player> registeredPlayers = new ArrayList<Player>();
	
	public Channel(String name, String range, String format, boolean allowInLockdown, boolean isPrivate, boolean isDefault) {
		this.name = name;
		this.range = range;
		this.allowInLockdown = allowInLockdown;
		this.isPrivate = isPrivate;
		this.format = format;
		this.isDefault = isDefault;
	}
	
	public void reName(String newName) {
		name = newName;
	}

	public void setRange(String newRange) {
		range = newRange;
	}

	public void togglePrivate() {
		if (isPrivate) {
			isPrivate = false;
		} else {
			isPrivate = true;
		}
	}

	public void toggleAllowInLockdown() {
		if (allowInLockdown) {
			allowInLockdown = false;
		} else {
			allowInLockdown = true;
		}
	}

	public void registerPlayer(Player player) {
		if (!registeredPlayers.contains(player)) {
			registeredPlayers.add(player);
		}
	}

	public void unregisterPlayer(Player player) {
		if (registeredPlayers.contains(player)) {
			registeredPlayers.remove(player);
		}
	}

	public boolean isDefault() {
		return isDefault;
	}

	public String getFormat() {
		return format;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public String getName() {
		return name;
	}

	public String getRange() {
		return range;
	}

	public boolean isAllowedInLockdown() {
		return allowInLockdown;
	}

	public List<Player> getRegisteredPlayers() {
		return registeredPlayers;
	}
	
	public int getRangeForWorld(String worldName){
		String[] worlds = range.split("|");
		int rangeBlocks = 0;
		for (String world : worlds) {
			//This world contains block range.
			if(world.contains("+")){
				String[] blocks = world.split("+");
				String range = blocks[1].replaceFirst("b", "");
				try{
					rangeBlocks = Integer.parseInt(range);
				}catch(Exception exception){
					Messenger.showDebug("[Channels] Range could not be parsed into numbers.. verify your range again.");
				}
			}
		}
		return rangeBlocks;
	}
	
	public List<String> getRangeWorlds() {
		List<String> worlds = new ArrayList<String>();
		
		String[] temp = range.split("|");
		for (String s : temp) {
			//This world contains ranges.
			if(s.contains("+")){
				String[] blocks = s.split("+");
				worlds.add(blocks[0].replaceFirst("w", ""));
			}
			
			worlds.add(s.replaceFirst("w", ""));
		}
		return worlds;
	}
}