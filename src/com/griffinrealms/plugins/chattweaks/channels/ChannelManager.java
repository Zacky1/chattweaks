package com.griffinrealms.plugins.chattweaks.channels;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.managers.Messenger;

public class ChannelManager {

	private Manager manager;
	private String defaultChannel;
	private HashMap<String, Channel> registeredChannels = new HashMap<String, Channel>();

	public ChannelManager(Manager man) {
		this.manager = man;
	}

	public void load(){
		
		defaultChannel = manager.getMainConfig().getString("Channels.Default Channel");
		
		ConfigurationSection chsec = (ConfigurationSection) manager.getMainConfig().get("Channels.Channels");
		if (chsec != null) {
			for (String key : chsec.getKeys(false)) {
				if (key != null) {
					String range =  manager.getMainConfig().getString("Channels.Channels."+ key + ".Range");
					String format =  manager.getMainConfig().getString("Channels.Channels." + key + ".Format");
					boolean privateCh =  manager.getMainConfig().getBoolean("Channels.Channels." + key + ".Private");
					boolean allowedInLockdown =  manager.getMainConfig().getBoolean("Channels.Channels."+ key + ".AllowInLockdown");
					boolean isDefault = false;
					
					if(defaultChannel.equals(key)){
						isDefault = true;
					}
					Channel channel = new Channel(key, range, format, allowedInLockdown, privateCh, isDefault);
					addChannel(channel);
				}
			}
			Messenger.showDebug("[Configuration]Enabled Channels. Found "+ registeredChannels.size()+ " different Channels!");
		}
	}
	
	public void addChannel(Channel channel){
		if(!registeredChannels.containsKey(channel.getName())){
			registeredChannels.put(channel.getName(), channel);
		}
	}
	
	public void removeChannel(Channel channel){
		if(registeredChannels.containsKey(channel.getName())){
			for(Player player : channel.getRegisteredPlayers()){
				unregisterPlayer(channel, player);
				break;
			}
			registeredChannels.remove(channel.getName());
		}
	}
	
	public void registerPlayer(Channel channel, Player player) {
		if (!channel.getRegisteredPlayers().contains(player)) {
			if (channel.isPrivate()) {
				for (Player s : channel.getRegisteredPlayers()) {
					manager.getMessager().showMessage(s.getName(), "Channel.Private.Join", player.getName());
					manager.getMessager().sendChannelJoin(s.getName());
				}
			}
			manager.getMessager().showMessage(player.getName(), "Channel.Joined", channel.getName());
			manager.getMessager().sendChannelJoin(player.getName());
			channel.registerPlayer(player);
		}
	}
	
	public void unregisterPlayer(Channel channel, Player player) {
		if (channel.getRegisteredPlayers().contains(player)) {
			if (channel.isPrivate()) {
				for (Player s : channel.getRegisteredPlayers()) {					
					manager.getMessager().showMessage(s.getName(), "Channel.Private.Leave", player.getName());
					manager.getMessager().sendChannelJoin(s.getName());
				}
			}
			manager.getMessager().showMessage(player.getName(), "Channel.Leave", channel.getName());
			manager.getMessager().sendChannelJoin(player.getName());
			channel.unregisterPlayer(player);
		}
	}
	
	public void unregisterPlayerFromAll(Player player) {
		for(Channel channel : registeredChannels.values()){
			if(channel.getRegisteredPlayers().contains(player)){
				unregisterPlayer(channel, player);
				return;
			}
		}
	}
	
	public Channel getDefaultChannel(){
		if(defaultChannel != null){
			return getChannelByName(defaultChannel);
		}
		return null;
	}
	
	public HashMap<String, Channel> getRegisteredChannels() {
		return registeredChannels;
	}
	
	public Channel getChannelByName(String channelName) {
		return registeredChannels.get(channelName);
	}

	public Channel getChannelByPlayer(Player player) {
		for(Channel channel : registeredChannels.values()){
			for(Player chPlayer : channel.getRegisteredPlayers()){
				if(chPlayer.equals(player)){
					return channel;
				}
			}
		}
		return null;
	}

	public String getJoinable(Player player, Channel channel) {
		if (channel.isPrivate()) {
			if (player.hasPermission("ChatTweaks.Channels." + channel.getName() + ".Join")) {
				return ChatColor.DARK_GREEN + "Joinable";
			} else {
				return ChatColor.DARK_RED + "Unjoinable";
			}
		} else {
			return ChatColor.DARK_GREEN + "Joinable";
		}
	}
	
	public void processChannelMessage(AsyncPlayerChatEvent event, boolean chatInLockdown){
		Player player = event.getPlayer();
		Channel channel = getChannelByPlayer(player);
		
		if(channel == null){
			event.setCancelled(false);
			return;
		}
		if(chatInLockdown){
			if(!channel.isAllowedInLockdown()){
				event.setCancelled(true);
				return;
			}
		}
		
		String format = channel.getFormat();
		event.getRecipients().clear();
		event.getRecipients().addAll(findRecipients(player, channel));
		
		format = format.replaceFirst("%playername%", "%s");
		format = format.replaceFirst("%message%", "%s");
		
		format = ChatColor.translateAlternateColorCodes('&', format);
		event.setFormat(format);
	}

	public boolean testDistance(Player player1, Player player2, Channel channel) {
		
		String world1 = player1.getWorld().getName();
		String world2 = player2.getWorld().getName();
		Location loc1 = player1.getLocation();
		Location loc2 = player2.getLocation();
		String range = channel.getRange();
		
		if(range.equalsIgnoreCase("INFINITE")){
			return true;
		}
		
		//Players are in the same world
		if(world1.equals(world2)){
		
			//If the world is specified in range, check if players are in range of each other.
			if(channel.getRangeWorlds().contains(world1)){
				int worldRange = channel.getRangeForWorld(world1);
				//World range is unlimited
				if(worldRange == 0){
					return true;
				}
				
				if(loc1.distance(loc2) <= worldRange){
					return true;
				}
			}else{
				if(range.startsWith("b")){
					int rangeBlocks = 0;
					try{
						Integer.parseInt(range.replaceFirst("b", ""));
					}catch(Exception e){
						Messenger.showDebug("[Channels] Problem parsing range into a number. Check your range in config!");
					}
					
					if(loc1.distance(loc2) <= rangeBlocks){
						return true;
					}
				}//Else if the range contains a world...
			}
		}
	
		if(channel.getRangeWorlds().contains(world1)){
			if(channel.getRangeForWorld(world1) == 0){
				if(channel.getRangeWorlds().contains(world2)){
					if(channel.getRangeForWorld(world2) == 0){
						return true;
					}
				}
			}
		}
		
		return false;
	}

	public Set<Player> findRecipients(Player player, Channel channel) {
		Set<Player> recipients = new HashSet<Player>();
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("ChatTweaks.Channels." + channel.getName() + ".Spy")) {
				recipients.add(p);
			}
			
			if(testDistance(player, p, channel)){
				if(!recipients.contains(p)){
					recipients.add(p);
				}
			}
		}
		recipients.add(player);
		return recipients;
	}
}
