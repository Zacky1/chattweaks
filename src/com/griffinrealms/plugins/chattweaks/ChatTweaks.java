package com.griffinrealms.plugins.chattweaks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.griffinrealms.plugins.chattweaks.commands.ChannelCmd;
import com.griffinrealms.plugins.chattweaks.commands.ChatTweaksCmd;
import com.griffinrealms.plugins.chattweaks.managers.DeathManager;
import com.griffinrealms.plugins.chattweaks.managers.Manager;
import com.griffinrealms.plugins.chattweaks.metrics.PlMetrics;
import com.griffinrealms.plugins.chattweaks.utils.Helper;

public class ChatTweaks extends JavaPlugin {

	public static ChatColor primaryColor = ChatColor.DARK_PURPLE;
	public static ChatColor secondaryColor = ChatColor.GOLD;
	public static final String prefix = primaryColor + "[" + secondaryColor + "ChatTweaks" + primaryColor + "]" + secondaryColor;
	public static final String rawPrefix = "[ChatTweaks]";
	public static boolean debug = false;


	public Helper helper;
	public boolean hasUpdate = false;
	public String currentversion;
	public String newversion;

	private PlMetrics plm;

	private Manager manager;

	public void onEnable(){
		PluginDescriptionFile version = getDescription();
		currentversion = "v"+version.getVersion();
		PluginManager pm = Bukkit.getPluginManager();
		manager = new Manager(this);
		helper = new Helper(this);
		plm = new PlMetrics(manager);
		
		if(manager.getMainConfig().getBoolean("Misc.Enable Update Checks")){
			helper.updateThread();
		}
		
		getCommand("ChatTweaks").setExecutor(new ChatTweaksCmd(manager));
		getCommand("Channels").setExecutor(new ChannelCmd(manager));
		
		pm.registerEvents(manager.getPlayers(), this);	
		pm.registerEvents(manager.getChat(), this);
		
		if(manager.getMainConfig().getBoolean("Misc.Enable Death Messages")) {
			pm.registerEvents(new DeathManager(this), this);
		}
		if(manager.getMainConfig().getBoolean("Misc.Use Metrics")){
			plm.main(true);
		}
	}

	public void onDisable() {
		Bukkit.getScheduler().cancelTasks(this);
	}

	public int toInteger(String str) {
		int ret = 0;
		try {
			ret = Integer.parseInt(str.replaceAll("[^0-9]", ""));
		} catch (Exception ex) {
			manager.getMessager().showMessage("CONSOLE", "ERROR",
					"Error while verifying version! Ex:" + ex);
		}
		return ret;
	}
}
